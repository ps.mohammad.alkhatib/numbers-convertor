package com.progress.assignments;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

public class NumberConvertorTest {


    @Test
    public void givenNullNumber_WhenConvert_ThenExceptionReturned(){
        NumbersConvertor numbersConvertor = new NumbersConvertor();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> numbersConvertor.convert(null));
        Assertions.assertEquals(exception.getMessage(),"number is null");

    }

    @Test
    public void givenNegativeNumber_whenConvert_ThenExceptionReturned(){
        NumbersConvertor numbersConvertor = new NumbersConvertor();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> numbersConvertor.convert(-3));
        Assertions.assertEquals(exception.getMessage(),"number is negative");
    }

    @Test
    public void givenOneDigitNumber_whenCovert_thenExpectedReturned(){
        NumbersConvertor numbersConvertor = new NumbersConvertor();
        Integer num = 5;
        String expectedResult = "five";
        String actualResult = numbersConvertor.convert(num);
        Assertions.assertEquals(expectedResult,actualResult);

    }
    @Test
    public void givenTwoDigitNumber_whenConvert_thenExpectedReturned(){
        NumbersConvertor convertor = new NumbersConvertor();
        Integer num = 56;
        String expected = "fifty six";
        String actual = convertor.convert(num);
        Assertions.assertEquals(expected,actual);
        expected = "fourteen";
        actual = convertor.convert(14);
        Assertions.assertEquals(expected,actual);
        expected = "twenty ";
        actual = convertor.convert(20);
        Assertions.assertEquals(expected,actual);
        expected = "eighty five";
        actual= convertor.convert(85);
        Assertions.assertEquals(expected,actual);
    }

    @Test
    public void givenThreeDigitNumber_whenConvert_thenExpectedReturned(){
        NumbersConvertor convertor = new NumbersConvertor();
        String expected = "one hundred ";
        String  actual = convertor.convert(100);
        Assertions.assertEquals(expected,actual);
        expected = "three hundred and twenty one";
        actual=convertor.convert(321);
        Assertions.assertEquals(expected,actual);
        expected = "seven hundred and thirty two";
        actual = convertor.convert(732);
        Assertions.assertEquals(expected,actual);
    }
    @Test
    public void givenFourDigitNumber_whenConvert_thenExpectedReturned(){
        NumbersConvertor convertor = new NumbersConvertor();
        String expected = "one thousand ";
        String  actual = convertor.convert(1000);
        Assertions.assertEquals(expected,actual);
        expected = "one thousand and six hundred and fifty four";
        actual = convertor.convert(1654);
        Assertions.assertEquals(expected,actual);
    }

    @Test
    public void givenFiveDigitNumber_whenConvert_thenExpectedReturned(){
        NumbersConvertor convertor = new NumbersConvertor();
        String expected = "ten  thousand ";
        String  actual = convertor.convert(10000);
        Assertions.assertEquals(expected,actual);
        expected = "eighteen thousand and six hundred and fifty four";
        actual = convertor.convert(18654);
        Assertions.assertEquals(expected,actual);
        expected = "ninety nine thousand ";
        actual = convertor.convert(99000);
        Assertions.assertEquals(expected,actual);
    }
    @Test
    public void givenSixDigitNumber_whenConvert_thenExpectedReturned(){
        NumbersConvertor convertor = new NumbersConvertor();
        String expected = "one hundred  thousand ";
        String  actual = convertor.convert(100000);
        Assertions.assertEquals(expected,actual);
        expected = "one hundred and fifty two thousand and six hundred and sixty six";
        actual = convertor.convert(152666);
        Assertions.assertEquals(expected,actual);
    }
    @Test
    public void givenSevenDigitNumber_whenConvert_thenExpectedReturned(){
        NumbersConvertor convertor = new NumbersConvertor();
        String expected = "one million ";
        String  actual = convertor.convert(1000000);
        Assertions.assertEquals(expected,actual);
        expected = "seven million and one hundred and fifty two thousand and six hundred and sixty six";
        actual = convertor.convert(7152666);
        Assertions.assertEquals(expected,actual);
    }
    @Test
    public void givenEightDigitNumber_whenConvert_thenExpectedReturned(){
        NumbersConvertor convertor = new NumbersConvertor();
        String expected = "eleven million ";
        String  actual = convertor.convert(11000000);
        Assertions.assertEquals(expected,actual);
        expected = "seventy two million and one hundred and fifty two thousand and six hundred and sixty six";
        actual = convertor.convert(72152666);
        Assertions.assertEquals(expected,actual);
    }
    @Test
    public void givenNineDigitNumber_whenConvert_thenExpectedReturned(){
        NumbersConvertor convertor = new NumbersConvertor();
        String expected = "one hundred  million ";
        String  actual = convertor.convert(100000000);
        Assertions.assertEquals(expected,actual);
        expected = "nine hundred and ninety nine million and nine hundred and ninety " +
                "nine thousand and nine hundred and ninety nine";
        actual = convertor.convert(999999999);
        Assertions.assertEquals(expected,actual);
    }

}
