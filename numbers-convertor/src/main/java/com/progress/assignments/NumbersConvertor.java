package com.progress.assignments;

public class NumbersConvertor {

    private final Data data;

    public NumbersConvertor() {
        this.data = new Data();
    }

    public String convert(Integer number) {
        if (number == null)
            throw new IllegalArgumentException("number is null");
        if (number < 0)
            throw new IllegalArgumentException("number is negative");
        return handleNum(number);
    }

    private int digitNum(Integer number) {
        String num = number.toString();
        return num.length();
    }

    private String handleNum(Integer num) {
        if (digitNum(num) == 1)
            return data.getUnit(num);
        if (digitNum(num)==2){
            return handleTenths(num);
        }
        if (digitNum(num) == 3){
            return handleHundreds(num);
        }
        return handleComplexNums(num);
    }

    private String handleHundreds(Integer num) {
        String number = num.toString();
        num = num %100;
        return handleNum(firstIndexNum(number)) +
                " hundred " + appendAnd(num) + handleNum(num);
    }

    private String handleComplexNums(Integer num) {
        if (isThousands(num)){
            int temp = num /1000;
            num = num %1000;
            return handleNum(temp) +
                    " thousand " +appendAnd(num)+ handleNum(num);
        }
        if (isMillions(num)){
            int temp = num /1000000;
            num = num %1000000;
            return handleNum(temp) +
                    " million " +appendAnd(num)+ handleNum(num);
        }
        return null;
    }

    private boolean isThousands(Integer num) {
        return digitNum(num) > 3 && digitNum(num) < 7;
    }

    private boolean isMillions(Integer num) {
        return digitNum(num) > 6 && digitNum(num) < 10;
    }

    private int firstIndexNum(String number){
       return   ((int)number.charAt(0)-48);
    }

    private String handleTenths(Integer num){
        String number = num.toString();
        num = num%10;
        if (isTeens(num, number))
            return data.getTeen(num);
        return data.getTenth(firstIndexNum(number)) + " " +handleNum(num);
    }

    private boolean isTeens(Integer num, String number) {
        return num != 0 && firstIndexNum(number) == 1;
    }
    private String appendAnd(int num){
        return num == 0? "" : "and ";
    }
}
