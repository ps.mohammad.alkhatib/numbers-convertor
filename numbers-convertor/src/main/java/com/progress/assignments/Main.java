package com.progress.assignments;

import java.util.HashMap;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 500; i++) {
            Integer a = new Random().nextInt(9999);
            NumbersConvertor numbersConvertor = new NumbersConvertor();
            String result = numbersConvertor.convert(a);
            System.out.println(a +": "+result);
        }
    }

}