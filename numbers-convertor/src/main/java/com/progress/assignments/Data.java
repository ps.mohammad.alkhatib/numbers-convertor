package com.progress.assignments;

import java.util.HashMap;

public class Data {

    private final HashMap<Integer,String> unitsMap = prepareUnitsMap();
    private final HashMap<Integer,String> tensMap = prepareTensMap();

    private final HashMap<Integer,String> teensMap = prepareTeensMap();

    public String getUnit(Integer num){
        return unitsMap.get(num);
    }

    public String getTenth(Integer num){
        return tensMap.get(num);
    }
    public String getTeen(Integer num){
        return teensMap.get(num);
    }
    private HashMap<Integer, String> prepareTeensMap(){
        HashMap<Integer, String> map = new HashMap<>();
        map.put(1,"eleven");
        map.put(2,"twelve");
        map.put(3,"thirteen");
        map.put(4,"fourteen");
        map.put(5,"fifteen");
        map.put(6,"sixteen");
        map.put(7,"seventeen");
        map.put(8,"eighteen");
        map.put(9,"nineteen");
        return map;
    }
    public  static HashMap<Integer, String> prepareUnitsMap(){
        HashMap<Integer, String> map = new HashMap<>();
        map.put(0,"");
        map.put(1,"one");
        map.put(2,"two");
        map.put(3,"three");
        map.put(4,"four");
        map.put(5,"five");
        map.put(6,"six");
        map.put(7,"seven");
        map.put(8,"eight");
        map.put(9,"nine");
        return map;
    }
    public  static HashMap<Integer, String> prepareTensMap(){
        HashMap<Integer, String> map = new HashMap<>();
        map.put(1,"ten");
        map.put(2,"twenty");
        map.put(3,"thirty");
        map.put(4,"forty");
        map.put(5,"fifty");
        map.put(6,"sixty");
        map.put(7,"seventy");
        map.put(8,"eighty");
        map.put(9,"ninety");
        return map;
    }
}
